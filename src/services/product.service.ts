import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ProductDTO } from 'src/dtos/product.DTO';
import { ProductRepository } from 'src/repositories/product.repository';

@Injectable()
export class ProductService {
    constructor(@InjectRepository(ProductRepository) private readonly productRepo: ProductRepository) { }

    async getProducts(){
        return await this.productRepo.find();
    }

    async getProduct(id){
        return await this.productRepo.findOne(id);
    }

    async createProduct(dto: ProductDTO){
        return await this.productRepo.save(dto);
    }

    async updateProduct(id: string, dto: ProductDTO){
        return await this.productRepo.update(id,dto);
    }

    async deleteProduct(id){
        return await this.productRepo.delete(id);
    }
}
