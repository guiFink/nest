import { Module } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { typeOrmConfig } from './config/typeorm.config';
import { UserModule } from './modules/user/user.module';
import { ProductModule } from './modules/product/product.module';
import { MailModule } from './modules/mail/mail.module';
import { MailService } from './services/mail.service';

@ApiTags('App Module')
@Module({
  imports: [
    TypeOrmModule.forRoot(typeOrmConfig), 
    UserModule, 
    ProductModule, MailModule
  ],
  controllers: [AppController],
  providers: [AppService, MailService],
})
export class AppModule {}
