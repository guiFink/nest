import { Body, Controller, Get, HttpCode, Param, ParseIntPipe, Post, Put, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiForbiddenResponse, ApiOkResponse, ApiResponse, ApiTags } from '@nestjs/swagger';
import { EditUserDTO } from 'src/dtos/editUser.DTO';
import { UserDTO } from 'src/dtos/user.DTO';
import { User } from 'src/entities/user.entity';
import { MailService } from 'src/services/mail.service';
import { UserService } from 'src/services/user.service';

@ApiTags('User Endpoint')
@Controller('user')
export class UserController {
    constructor(private userService: UserService, private mailService: MailService) { }

    
    @Get()
    @ApiOkResponse({description: 'Devuelve una lista de usuarios'})
    @ApiForbiddenResponse({description: 'Forbidden'})
    async getUsers() {
        const data = await this.userService.getUsers();
        return {
            estado: 'Peticion ok',
            data
        }
    }

    @Get(':id')
    async getUserById(@Param('id') id: number) {
        return this.userService.getUserById(id);
    }


    @Post()
    @HttpCode(200)
    @UsePipes(ValidationPipe)
    crearUsuario(@Body() user: UserDTO) {
        try {
            const rta = this.userService.createUser(user);
            return rta;
        } catch (e) {
            return e;
        }


    }

    @Put(':id')
    editUser(@Param('id') id: ParseIntPipe, @Body() dto: EditUserDTO) {
        console.log(id);
        return dto;
    }

    @Post('confirmation')
    async confirmarUsuario(){
        const user = {
            nombre: "Guillo",
            mail: "guillermo.fink@reserv.com.ar"
        };
        await this.mailService.sendUserConfirmation(user, "");
        return {msg: 'mail ok'};
    }
}
