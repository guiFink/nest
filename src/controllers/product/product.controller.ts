import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ProductDTO } from 'src/dtos/product.DTO';
import { ProductResponse } from 'src/responses/product.response';
import { ProductService } from 'src/services/product.service';

@ApiTags('Product Endpoint')
@Controller('product')
export class ProductController {

    constructor(private readonly productService: ProductService){}

    @Get()
    getProducts() {
        return this.productService.getProducts();
    }

    @Get(':id')
    async getProduct(@Param('id') id: number) {
        const prod = await this.productService.getProduct(id);
        console.log(prod);
        const response: ProductResponse = new ProductResponse(prod.descripcion, prod.precio);
        return response;
    }

    @Post()
    crearProducto(@Body() productoDTO: ProductDTO) {
        return this.productService.createProduct(productoDTO);
    }

    @Put(':id')
    updateProducto(@Param('id') id: string, @Body() productoDTO: any) {
        return this.productService.updateProduct(id,productoDTO);
    }

    @Delete(':id')
    deleteProducto(@Param('id') id: number) {
        return this.productService.deleteProduct(id);
    }
}
