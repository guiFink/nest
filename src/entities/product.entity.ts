import { ApiProperty } from "@nestjs/swagger";
import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('products')
export class Product extends BaseEntity{

    @PrimaryGeneratedColumn({
        comment: 'id de producto'
    })
    @ApiProperty({
        type: Number,
        description: 'Id del producto'
    })
    id: number;

    @Column({
        type: 'varchar',
        length: 70
    })
    @ApiProperty({
        type: String,
        description: 'Descripción del producto'
    })
    descripcion: string;

    @Column({
        type: "double"
    })
    @ApiProperty({
        type: Number,
        description: 'Valor del producto'
    })
    precio: number;

    
}