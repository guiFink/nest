import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserController } from 'src/controllers/user/user.controller';
import { User } from 'src/entities/user.entity';
import { UserRepository } from 'src/repositories/user.repository';
import { MailService } from 'src/services/mail.service';
import { UserService } from 'src/services/user.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([User, UserRepository])
    ],
    controllers: [
        UserController
    ],
    providers: [
        UserService, MailService
    ]
})
export class UserModule {}
