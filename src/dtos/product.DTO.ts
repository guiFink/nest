import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class ProductDTO {

    @ApiProperty({
        type: Number,
        description: 'Id del producto'
    })
    id: number;

    @IsNotEmpty({ message: 'La descripción del producto no puede ser nula' })
    @ApiProperty({
        type: String,
        description: 'Descripción del producto'
    })
    descripcion: string;

    @IsNotEmpty()
    @ApiProperty({
        type: Number,
        description: 'Precio del producto'
    })
    precio: number;
}