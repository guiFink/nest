import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, Length } from "class-validator";

export class UserDTO {
    @IsNotEmpty({message: "El nombre no puede ser nulo"})
    @ApiProperty({
        type: String,
        description: 'Nombre de usuario'
    })
    nombre: string;

    @IsNotEmpty({message: 'El apellido no puede ser nulo'})
    @Length(3,50)
    @ApiProperty({
        type: String,
        description: 'Apellido del usuario'
    })
    apellido: string;
    

    @IsNotEmpty({message: 'La edad no puede ser nula'})
    @ApiProperty({
        type: Number,
        description: 'Edad del usuario'
    })
    edad: number;
}