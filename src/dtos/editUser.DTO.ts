import { PartialType } from "@nestjs/mapped-types";
import { ApiProperty, OmitType } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";
import { UserDTO } from "./user.DTO";

export class EditUserDTO extends PartialType( OmitType(UserDTO, ['edad'] as const) ) {
    @IsNotEmpty({message: 'El id de usuario no puede ser nulo'})
    @ApiProperty({
        type: Number,
        description: 'Id del usuario'
    })
    id: number;
}